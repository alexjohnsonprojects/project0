package menus;

import exceptions.UnexpectedSwitchCaseException;
import users.Customer;
import users.User;

class CustomerMainMenu extends Menu {

	private Customer loggedInCustomer;
	
	CustomerMainMenu(User loggedInUser) {
		super(MenuType.CUSTOMER_MAIN);
		this.loggedInCustomer = (Customer) loggedInUser;
	}
	
	@Override
	public MenuReturn doMenuAction(int selection) {
		Menu M;
		switch(selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			return MenuReturn.GOHOME;
		case 2:
			M = new AccountSelectionMenu(loggedInCustomer);
			return M.doAbstractMenuAction();
		case 3:
			M = new CustomerAccountApplicationCreationMenu(loggedInCustomer);
			return M.doAbstractMenuAction();
		case 4:
			M = new AccountApplicationSelectionMenu(loggedInCustomer);
			return M.doAbstractMenuAction();
		default:
			throw new UnexpectedSwitchCaseException();
		}
	}
	

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Customer Main Menu");
		menuSubTitle = new StringBuffer("Welcome " + loggedInCustomer.getDisplayName() + "\n");
	}
	
	@Override
	public void populateOptions() {
		addOption("Logout");
		addOption("Manage Existing Accounts");
		addOption("Apply for a New Account");
		addOption("View Open Applications");
	}
}
