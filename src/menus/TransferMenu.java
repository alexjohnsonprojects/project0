package menus;

import java.util.List;

import bankingaccounts.Account;
import bankingaccounts.AccountDAO;
import bankingaccounts.AccountOracleDAO;
import exceptions.EmptyCancellationException;
import exceptions.InsufficientBalanceException;
import exceptions.NonpositiveException;


class TransferMenu extends Menu {

	private List<Account> targetAccounts;
	private Account sourceAccount;

	TransferMenu(Account sourceAccount, List<Account> targetAccounts) {
		super(MenuType.TRANSFER);
		this.sourceAccount = sourceAccount;
		this.targetAccounts = targetAccounts;
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		switch (selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			return MenuReturn.BACK;
		default:
			try {
				doTransfer(targetAccounts.get(selection - 2));
			} catch (EmptyCancellationException e) {
				System.out.println(e.getMessage());
				return MenuReturn.STAY;
			}
			return MenuReturn.BACK;
		}
	}

	private void doTransfer(Account targetAccount) throws EmptyCancellationException {
		boolean goodTransfer = false;
		while (!goodTransfer) {
			System.out.println("Enter the amount you would like to transfer.");
			double transferAmount = MenuMasterControl.getUserInputDouble();
			try {
				double oldSourceBalance = sourceAccount.getBalance();
				double oldTargetBalance = targetAccount.getBalance();
				Account.doTransfer(transferAmount, sourceAccount, targetAccount);
				AccountDAO adao = new AccountOracleDAO();
				if(adao.updateAccount(sourceAccount) && adao.updateAccount(targetAccount)) {
					System.out.println("Transfer successful.");
				} else {
					sourceAccount.setBalance(oldSourceBalance);
					targetAccount.setBalance(oldTargetBalance);
					System.out.println("Transfer failed.");
				}
				goodTransfer = true;
			} catch (NonpositiveException | InsufficientBalanceException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	@Override
	public void populateOptions() {
		addOption("Go Back");
		for (Account acc : targetAccounts) {
			addOption("Account: " + acc.getDisplayString());
		}
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Balance Transfer Menu");
		menuSubTitle = new StringBuffer("Select a Target Account to Initiate Transfer from Source Account "
				+ sourceAccount.getDisplayString() + "\n");
	}
}
