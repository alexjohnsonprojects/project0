package menus;

import users.Employee;
import users.UserType;

class EmployeeAdminMainMenu extends Menu {
	
	private Employee loggedInEmployee;
	private UserType UType;
	
	EmployeeAdminMainMenu(Employee loggedInEmployee) {
		super(MenuType.CUSTOMER_MAIN);
		this.loggedInEmployee = loggedInEmployee;
		UType = loggedInEmployee.getUserType();
	}
	
	@Override
	public MenuReturn doMenuAction(int selection) {
		Menu M;
		switch(selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			return MenuReturn.GOHOME;
		case 2:
			M = new EmployeeSelectCustomerMenu(loggedInEmployee);
			return M.doAbstractMenuAction();
		case 3:
			M = new AccountSelectionMenu(loggedInEmployee);
			return M.doAbstractMenuAction();
		case 4:
			M = new AccountApplicationSelectionMenu(loggedInEmployee);
			return M.doAbstractMenuAction();
		default:
		}
		return MenuReturn.ERROR;
	}

	@Override
	public void populateOptions() {
		addOption("Logout");
		addOption("View All Customer Information");
		String viewOrManage;
		switch(UType) {
		default:
		case EMPLOYEE:
			viewOrManage = "View";
			break;
		case ADMIN:
			viewOrManage = "Manage";
			break;
		}
		addOption(viewOrManage + " All Banking Accounts");
		addOption("Manage Open Banking Account Applications");
	}

	@Override
	public void generateTitles() {
		String title = "";
		switch(loggedInEmployee.getUserType()) {
		default:
		case EMPLOYEE:
			title = "Employee Main Menu";
			break;
		case ADMIN:
			title = "Admin Main Menu";
			break;
		}
		menuTitle = new StringBuffer(title);
		menuSubTitle = new StringBuffer("Welcome " + loggedInEmployee.getUsername() + "\n");
	}
}
