package menus;

import java.util.ArrayList;
import java.util.List;

import bankingaccounts.Account;
import bankingaccounts.AccountDAO;
import bankingaccounts.AccountOracleDAO;
import bankingaccounts.AccountStatus;
import exceptions.DepositTooBigException;
import exceptions.EmptyCancellationException;
import exceptions.InsufficientBalanceException;
import exceptions.MissingAccountException;
import exceptions.NonpositiveException;
import exceptions.UnexpectedSwitchCaseException;
import exceptions.WithdrawTooBigException;
import users.Customer;
import users.User;
import users.UserDAO;
import users.UserOracleDAO;
import users.UserType;

class AccountManagementMenu extends Menu {

	private User loggedInUser;
	private Account selectedAccount;
	private List<Account> accessibleAccounts;
	private UserType UType;
	private AccountStatus AStatus;

	AccountManagementMenu(User loggedInUser, Account selectedAccount, List<Account> accessibleAccounts) {
		super(MenuType.ACCOUNT_MANAGEMENT);
		this.loggedInUser = loggedInUser;
		this.selectedAccount = selectedAccount;
		this.accessibleAccounts = accessibleAccounts;
		UType = loggedInUser.getUserType();
		AStatus = selectedAccount.getStatus();
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		switch (selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			return MenuReturn.BACK;
		default:
			int offset = 0;
			switch (UType) {
			case EMPLOYEE:
				System.out.println(
						"Employees shouldn't be able to get here! Something is wrong with their max selection...");
				break;
			case ADMIN:
				offset = 3;
				if (selection == 2) {
					// close, restore, or approve
					AccountDAO adao = new AccountOracleDAO();
					switch (AStatus) {
					case ACTIVE:
						selectedAccount.setStatus(AccountStatus.CLOSED);
						if(adao.updateAccount(selectedAccount)) {
							System.out.println("Account closed.");
						} else {
							selectedAccount.setStatus(AccountStatus.ACTIVE);
							System.out.println("Account closure failed");
						}
						break;
					case CLOSED:
						selectedAccount.setStatus(AccountStatus.ACTIVE);
						if(adao.updateAccount(selectedAccount)) {
							System.out.println("Account restored.");
						} else {
							selectedAccount.setStatus(AccountStatus.CLOSED);
							System.out.println("Account restoration failed");
						}
						break;
					case REJECTED:
						selectedAccount.setStatus(AccountStatus.ACTIVE);
						if(adao.updateAccount(selectedAccount)) {
							System.out.println("Application restored and approved.");
						} else {
							selectedAccount.setStatus(AccountStatus.REJECTED);
							System.out.println("Account restoration and approval failed");
						}
						break;
					default:
						throw new UnexpectedSwitchCaseException();
					}
					return MenuReturn.BACK;
				}
				break;
			case CUSTOMER:
				offset = 2;
				break;
			}
			int offsetSelection = selection - offset;
			switch (offsetSelection) {
			case 0:
				// make deposit
				try {
					makeDeposit();
				} catch (EmptyCancellationException e) {
					System.out.println(e.getMessage());
					return MenuReturn.STAY;
				}
				break;
			case 1:
				// withdraw funds
				if(selectedAccount.getBalance() == 0) {
					System.out.println("There is no money in this account to withdraw!");
					return MenuReturn.STAY;
				}
				try {
					makeWithdraw();
				} catch (EmptyCancellationException e) {
					System.out.println(e.getMessage());
					return MenuReturn.STAY;
				}
				break;
			case 2:
				// transfer funds to another account
				if(selectedAccount.getBalance() == 0) {
					System.out.println("There is no money in this account to transfer!");
					return MenuReturn.STAY;
				}
				List<Account> targetAccounts = new ArrayList<>();
				boolean foundAsExpected = false;
				for (Account acc : accessibleAccounts) {
					if (acc.equals(selectedAccount)) {
						foundAsExpected = true;
					} else if (acc.getStatus() == AccountStatus.ACTIVE) {
						targetAccounts.add(acc);
					}
				}
				if (!foundAsExpected) {
					throw new MissingAccountException();
				}
				if(targetAccounts.size() == 0) {
					//the selected account should always be among the accessible accounts...
					System.out.println("There are no other accounts to which to transfer!");
					return MenuReturn.STAY;
				}
				Menu M = new TransferMenu(selectedAccount, targetAccounts);
				return M.doAbstractMenuAction();
			}
			return MenuReturn.STAY;
		}
	}

	private void makeWithdraw() throws EmptyCancellationException {
		boolean goodWithdraw = false;
		while (!goodWithdraw) {
			System.out.println("Enter the amount you would like to withdraw, or enter blank to cancel.");
			double withdraw = MenuMasterControl.getUserInputDouble();
			try {
				Customer customerWithdrawee = null;
				if(UType == UserType.CUSTOMER) {
					customerWithdrawee = (Customer) loggedInUser;
					double oldWithdrawnToday = customerWithdrawee.getAmountWithdrawnToday();
				}
				double oldbalance = selectedAccount.getBalance();
				selectedAccount.makeWithdraw(withdraw, customerWithdrawee);
				goodWithdraw = true;
				AccountDAO adao = new AccountOracleDAO();
				UserDAO udao = new UserOracleDAO();
				udao.updateUser(customerWithdrawee);
				if(adao.updateAccount(selectedAccount)) {
					System.out.println("Withdraw successful.");
				} else {
					selectedAccount.setBalance(oldbalance);
					System.out.println("Withdraw failed.");
				}
			} catch (WithdrawTooBigException | NonpositiveException | InsufficientBalanceException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	private void makeDeposit() throws EmptyCancellationException {
		boolean goodDeposit = false;
		while (!goodDeposit) {
			System.out.println("Enter the amount you would like to deposit, or enter blank to cancel.");
			double deposit = MenuMasterControl.getUserInputDouble();
			try {
				double oldbalance = selectedAccount.getBalance();
				selectedAccount.makeDeposit(deposit);
				goodDeposit = true;
				AccountDAO adao = new AccountOracleDAO();
				if(adao.updateAccount(selectedAccount)) {
					System.out.println("Deposit successful.");
				} else {
					selectedAccount.setBalance(oldbalance);
					System.out.println("Deposit failed.");
				}
			} catch (DepositTooBigException | NonpositiveException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	@Override
	public void populateOptions() {
		addOption("Go Back");
		switch (UType) {
		default:
		case EMPLOYEE:
			// employees cannot do anything but look at the account
			break;
		case ADMIN:
			switch (AStatus) {
			case ACTIVE:
				addOption("Close Account");
				break;
			case CLOSED:
				addOption("Restore Account");
				break;
			case REJECTED:
				addOption("Restore and Approve Application");
				break;
			default:
				throw new UnexpectedSwitchCaseException();
			}
			// fall through logic to customer (if account is active)
		case CUSTOMER:
			if (AStatus == AccountStatus.ACTIVE) {
				addOption("Make Deposit");
				addOption("Withdraw Funds");
				addOption("Transfer Funds to Another Account");
			}
			break;
		}
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Account Management Menu");
		menuSubTitle = new StringBuffer("Account: " + selectedAccount.getDisplayString() + "\n");
	}
}
