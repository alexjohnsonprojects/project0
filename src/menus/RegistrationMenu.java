package menus;

import exceptions.*;
import users.*;

class RegistrationMenu extends Menu {
	
	private static final String ACCESS_CODE = "aaa";
	
	private static final int PASSWORD_MIN_LENGTH = 5;
	
	UserType UType;

	RegistrationMenu(UserType UType) {
		super(MenuType.REGISTER);
		this.UType=UType;
	}
	
	@Override
	public void generateTitles() {
		switch(UType) {
		default:
		case CUSTOMER:
			menuTitle = new StringBuffer("New Customer Registration");
			break;
		case EMPLOYEE:
			menuTitle = new StringBuffer("New Employee Registration");
			break;
		case ADMIN:
			menuTitle = new StringBuffer("New Admin Registration");
			break;
		}
		menuSubTitle = new StringBuffer("");
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		switch(UType) {
		case EMPLOYEE:
		case ADMIN:
			System.out.println("Please enter the access code provided by your manager. (For Demo: code = " + ACCESS_CODE + ")");
			boolean validated = false;
			try {
				validated = validateAccessCode(MenuMasterControl.getUserInputNonempty());
				if(!validated) {
					System.out.println("Incorrect access code.");
				}
			} catch (EmptyCancellationException e) {
				System.out.println(e.getMessage());
				return MenuReturn.BACK;
			}
			if(!validated) {
				return MenuReturn.BACK;
			}
			//fall through logic to customer account creation, this object remembers the user type
		case CUSTOMER:
			try {
				if(createNewUser()) {
					System.out.println("User creation successful.");
				} else {
					System.out.println("User creation failed.");
				}
			} catch (EmptyCancellationException e) {
				System.out.println(e.getMessage());
				return MenuReturn.BACK;
			}
			return MenuReturn.GOHOME;
		default:
			return MenuReturn.GOHOME;
		}
//		return MenuReturn.GOHOME;
	}
	
	private boolean validateAccessCode(String input) {
		String accessCode;
		switch(UType) {
		case EMPLOYEE:
			accessCode = getEmployeeAccessCode();
			break;
		case ADMIN:
			accessCode = getAdminAccessCode();
			break;
		case CUSTOMER:
		default:
			return false; //code should never reach this line...
		}
		return input.equals(accessCode);
	}
	
	private boolean createNewUser() throws EmptyCancellationException {
		//will loop here until the user is successfully created, OR will throw an exception to cancel
		boolean goodUsername = false;
		boolean goodPassword = false;
		boolean goodSSN = false;
		String username = "";
		String password = "";
		String ssn = "";
		while(!goodUsername) {
			System.out.println("Please enter a unique username containing no spaces. Enter blank to cancel.");
			try {
				username = getGoodUsername();
				goodUsername = true;
			} catch (ExistingUsernameException e) {
				System.out.println(e.getMessage());
			} catch (SpaceInInputException e) {
				System.out.println(e.getMessage("username"));
			}
		}
		while(!goodPassword) {
			System.out.println("Please enter a good password (at least " + PASSWORD_MIN_LENGTH + " characters and no spaces). Enter blank to cancel.");
			try {
				password = getGoodPassword();
				goodPassword = true;
			} catch (BadPasswordException e) {
				System.out.println(e.getMessage());
			} catch (SpaceInInputException e) {
				System.out.println(e.getMessage("password"));
			}
		}
		while(!goodSSN) {
			System.out.println("Please enter your SSN in the format XXX-XX-XXXX (including dashes). Enter blank to cancel.");
			try {
				ssn = getGoodSSN();
				goodSSN = true;
			} catch (BadSSNException|ExistingSSNException e) {
				System.out.println(e.getMessage());
			}
		}
		return User.createAndAddNewUser(username, password, ssn, UType);
	}
	
	private String getGoodSSN() throws BadSSNException, EmptyCancellationException, ExistingSSNException {
		String ssn = MenuMasterControl.getUserInputNonempty();
		if(!User.checkFormattedSSN(ssn)) {
			throw new BadSSNException();
		}
		if(User.ssnAlreadyExistsSameUserType(ssn, UType)) {
			throw new ExistingSSNException();
		}
		return ssn;
	}
	
	private String getGoodUsername() throws ExistingUsernameException, SpaceInInputException, EmptyCancellationException {
		String un = MenuMasterControl.getUserInputNonempty();
		if(un.contains(" ")) {
			throw new SpaceInInputException();
		}
		if(User.usernameAlreadyExists(un)) {
			throw new ExistingUsernameException();
		}
		return un;
	}
	
	private String getGoodPassword() throws BadPasswordException, SpaceInInputException, EmptyCancellationException {
		String pw = MenuMasterControl.getUserInputNonempty();
		if(pw.contains(" ")) {
			throw new SpaceInInputException();
		}
		if(pw.length() < PASSWORD_MIN_LENGTH) {
			throw new BadPasswordException();
		}
		return pw;
	}

	private String getEmployeeAccessCode() {
		return ACCESS_CODE;
	}
	
	private String getAdminAccessCode() {
		return ACCESS_CODE;
	}

	@Override
	public void populateOptions() {
		//no options in this menu
	}
}
