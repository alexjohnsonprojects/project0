package menus;

import users.UserType;

class NewUserMenu extends Menu {

	NewUserMenu() {
		super(MenuType.NEWUSER);
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Register New User");
		menuSubTitle = new StringBuffer("Are you a customer, employee, or admin?\n");
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		Menu M;
		switch(selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			return MenuReturn.GOHOME;
		case 2:
			M = new RegistrationMenu(UserType.CUSTOMER);
			return M.doAbstractMenuAction();
		case 3:
			M = new RegistrationMenu(UserType.EMPLOYEE);
			return M.doAbstractMenuAction();
		case 4:
			M = new RegistrationMenu(UserType.ADMIN);
			return M.doAbstractMenuAction();
		default:
			return MenuReturn.ERROR;
		}
	}

	@Override
	public void populateOptions() {
		addOption("Return Home");
		addOption("Customer");
		addOption("Employee");
		addOption("Admin");
	}

}
