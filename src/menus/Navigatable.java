package menus;

interface Navigatable {
	void populateOptions();
	void generateTitles();
	MenuReturn doMenuAction(int selection);
}
