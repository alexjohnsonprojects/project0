package menus;

import bankingaccounts.*;
import exceptions.*;
import users.*;

class CustomerAccountApplicationCreationMenu extends Menu {

	private AccountType AType;
	private Customer loggedInCustomer;

	CustomerAccountApplicationCreationMenu(Customer loggedInCustomer) {
		super(MenuType.ACCOUNT_APPLY);
		this.loggedInCustomer = loggedInCustomer;
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		switch (selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			return MenuReturn.BACK;
		case 2:
			AType = AccountType.CHECKING;
			break;
		case 3:
			AType = AccountType.JOINT_CHECKING;
			break;
		default:
			return MenuReturn.ERROR;
		}
		try {
			if (createNewAccountApplication(AType)) {
				System.out.println("Account application created successfully.");
			} else {
				System.out.println("Account application creation failed.");
			}

		} catch (EmptyCancellationException e) {
			System.out.println(e.getMessage());
			return MenuReturn.STAY;
		}
		return MenuReturn.BACK;
	}

	private boolean createNewAccountApplication(AccountType AType) throws EmptyCancellationException {
//		AccountID aid = null;
		Customer secondCustomer = null;
		switch (AType) {
		default:
		case CHECKING:
//			aid = Account.createNewAccount(loggedInCustomer, AccountStatus.APPLIED);
			break;
		case JOINT_CHECKING:
			boolean goodSecondUser = false;
			boolean correctSecondUserSSN = false;
			while (!goodSecondUser) {
				try {
					System.out.println(
							"Enter the username of the user with whom you would like to apply for a joint account, or enter blank to cancel.");
					secondCustomer = getSecondCustomer();
					goodSecondUser = true;
				} catch (NonexistingUsernameException | SecondUserIsSelfException e) {
					System.out.println(e.getMessage());
				}
			}
			while (!correctSecondUserSSN) {
				try {
					System.out.println("Please confirm the SSN of user " + secondCustomer.getUsername()
							+ ", or enter blank to cancel.");
					String secondUserSSN = getSecondUserSSN();
					if (!secondCustomer.getSSN().equals(secondUserSSN)) {
						System.out.println("Incorrect SSN!");
					} else {
						correctSecondUserSSN = true;
					}
				} catch (BadSSNException e) {
					System.out.println(e.getMessage());
				}
			}
			break;
		}
//		aid = Account.createNewAccount(loggedInCustomer, secondCustomer, AccountStatus.APPLIED);
		switch (AType) {
		case JOINT_CHECKING:
//			secondCustomer.addAccountID(aid);
//			loggedInCustomer.addAccountID(aid);
//			return DB.createNewAccountApplication(loggedInCustomer.getUsername(), secondCustomer.getUsername(), AType);
			Account A = new Account(loggedInCustomer, secondCustomer, AccountStatus.APPLIED);
			AccountDAO adao = new AccountOracleDAO();
			return adao.insertAccount(A);
		case CHECKING:
//			loggedInCustomer.addAccountID(aid);
//			return DB.createNewAccountApplication(loggedInCustomer.getUsername(), null, AType);
			Account B = new Account(loggedInCustomer, AccountStatus.APPLIED);
			AccountDAO adao2 = new AccountOracleDAO();
			return adao2.insertAccount(B);
		default:
			return false;
		}
	}

	private String getSecondUserSSN() throws EmptyCancellationException, BadSSNException {
		String ssn = MenuMasterControl.getUserInputNonempty();
		if (!User.checkFormattedSSN(ssn)) {
			throw new BadSSNException();
		}
		return ssn;
	}

	private Customer getSecondCustomer()
			throws EmptyCancellationException, NonexistingUsernameException, SecondUserIsSelfException {
		String secondUsername = MenuMasterControl.getUserInputNonempty();
		if (loggedInCustomer.getUsername().equals(secondUsername)) {
			throw new SecondUserIsSelfException();
		}
		if (!User.usernameAlreadyExists(secondUsername, UserType.CUSTOMER)) {
			throw new NonexistingUsernameException();
		}
		//checks database
//		DB.verifyUsernameExistsOfType(secondUsername, UserType.CUSTOMER);
		return (Customer) User.getUserByUsername(secondUsername);
	}

	@Override
	public void populateOptions() {
		addOption("Cancel Application");
		addOption("Personal Checking Account (Will create application immediately!)");
		addOption("Joint Checking Account (Will prompt for second user's information.)");
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Account Application Menu");
		menuSubTitle = new StringBuffer("For what kind of account would you like to apply?\n");
	}
}
