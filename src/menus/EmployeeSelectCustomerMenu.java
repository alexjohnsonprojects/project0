package menus;

import java.util.List;

import exceptions.UnexpectedSwitchCaseException;
import users.*;

class EmployeeSelectCustomerMenu extends Menu {

	private Employee loggedInEmployee;
	private List<User> allCustomers;
//	private UserType UType;

	EmployeeSelectCustomerMenu(Employee loggedInEmployee) {
		super(MenuType.EMPLOYEE_SELECT_CUSTOMER);
		this.loggedInEmployee = loggedInEmployee;
//		UType = loggedInEmployee.getUserType();
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		switch(selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			return MenuReturn.BACK;
		default:
			Customer selectedCustomer = (Customer) allCustomers.get(selection - 2);
			Menu M = new AccountSelectionMenu(loggedInEmployee, selectedCustomer);
			return M.doAbstractMenuAction();
		}
	}
	
	@Override
	public void populateOptions() {
		allCustomers = User.getAllUsersOfType(UserType.CUSTOMER);
		addOption("Go Back");
		for (User u : allCustomers) {
			addOption("Customer: " + ((Customer) u).getFullCustomerDisplayString());
		}
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Customer Selection Menu");
		menuSubTitle = new StringBuffer("");
		switch (loggedInEmployee.getUserType()) {
		case EMPLOYEE:
			menuSubTitle = new StringBuffer("Select a customer to view all their active banking accounts.");
			break;
		case ADMIN:
			menuSubTitle = new StringBuffer("Select a customer to access all their active banking accounts.");
			break;
		default:
			throw new UnexpectedSwitchCaseException();
		}
		menuSubTitle.append("\n");
	}
}
