package menus;

//import java.io.*;

import java.util.Scanner;

import bankingaccounts.AccountType;
import database.DB;
import exceptions.EmptyCancellationException;
import exceptions.EmptyRangeException;

public class MenuMasterControl {

	private static Scanner scan = new Scanner(System.in);
	private static final String USERDATA_PATH = "datafiles/userdata.txt";
	private static final String BANKINGACCOUNTDATA_PATH = "datafiles/bankingaccountdata.txt";
	private static final String DATAFOLDER_PATH = "datafiles";

	public static void main(String[] args) {
		System.out.println(AccountType.CHECKING);
		System.out.println("Application started.");
//		loadAllData();
		Menu M = new HomeMenu();
		while (M.doAbstractMenuAction() == MenuReturn.STAY) {
			// just loop until user exits the program
		}
//		saveAllData();
		DB.closeConn();
		scan.close();
		System.out.println("Application has shut down.");
	}

//	private static void loadAllData() {
//		System.out.println("Loading data...");
//		try {
//			serialization.Depersist.loadAll();
//			System.out.println("Data successfully loaded.");
//		} catch (IOException | ClassNotFoundException e) {
//			e.printStackTrace();
//		}
//	}
//
//	private static void saveAllData() {
//		System.out.println("Saving data...");
//		try {
//			serialization.Persist.saveAll();
//			System.out.println("Data successfully saved.");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
	public static double getUserInputDouble() throws EmptyCancellationException {
		double r = 0;
		boolean goodInput = false;
		while(!goodInput) {
			try {
				String input = getUserInput();
				if(input.equals("")) {
					throw new EmptyCancellationException();
				}
				r = Double.parseDouble(input);
				goodInput = true;
			} catch (IllegalArgumentException e) {
				System.out.println("Invalid Input! Please enter a double!");
			}
		}
		
		return r;
	}

	public static int getUserInputInt(int min, int max) {
		int r = 0;
		if (min > max) {
			throw new EmptyRangeException();
		}
		boolean goodInput = false;
		while(!goodInput) {
			try {
				String input = getUserInput();
				r = Integer.parseInt(input);
				if (r < min || r > max) {
					throw new IllegalArgumentException();
				}
				goodInput = true;
			} catch (IllegalArgumentException e) {
				System.out.println("Invalid Input! Please enter a whole number between " + min + " and " + max + " (inclusive).");
			}
		}
		return r;
	}

	private static String getUserInput() {
		return scan.nextLine();
	}

	public static String getUserInputNonempty() throws EmptyCancellationException {
		String r = scan.nextLine();
		if (r.equals("")) {
			throw new EmptyCancellationException();
		}
		return r;
	}

	public static String getUserdataPath() {
		return USERDATA_PATH;
	}

	public static String getBankingaccountdataPath() {
		return BANKINGACCOUNTDATA_PATH;
	}

	public static String getDatafolderPath() {
		return DATAFOLDER_PATH;
	}
}
