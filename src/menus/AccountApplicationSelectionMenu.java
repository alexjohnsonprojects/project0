package menus;

import java.util.ArrayList;
import java.util.List;

import bankingaccounts.Account;
import bankingaccounts.AccountStatus;
import users.Customer;
import users.User;
import users.UserType;

class AccountApplicationSelectionMenu extends Menu {

	private User loggedInUser;
	private UserType UType;
	
	private List<Account> openApplications;

	AccountApplicationSelectionMenu(User loggedInUser) {
		super(MenuType.ACCOUNT_APPLICATION_SELECTION);
		this.loggedInUser = loggedInUser;
		UType = loggedInUser.getUserType();
	}

	@Override
	public void populateOptions() {
		openApplications = new ArrayList<>();
		addOption("Go Back");
		AccountStatus[] applied = {AccountStatus.APPLIED};
		switch (UType) {
		case CUSTOMER:
//			openApplications = Account.getUsersApplications(loggedInUser);
			openApplications = Account.getAccounts((Customer) loggedInUser, applied);
			break;
		case EMPLOYEE:
		case ADMIN:
		default:
			openApplications = Account.getAccounts(null, applied);
			break;
		}
		for (Account A : openApplications) {
			addOption("Application: " + A.getApplicationDisplayString());
		}
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Banking Account Application Management Menu");
		switch (UType) {
		case CUSTOMER:
			menuSubTitle = new StringBuffer(
					"Below you can view open or rejected applications. Selecting them currently has no function.\n");
			break;
		case EMPLOYEE:
		case ADMIN:
			menuSubTitle = new StringBuffer("Select an open account application to approve or reject. Only applications awaiting review are shown.\n");
			break;
		}
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		switch(selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			return MenuReturn.BACK;
		default:
			if(UType == UserType.CUSTOMER) {
				System.out.println("Selecting applications currently has no function as a customer.");
				return MenuReturn.STAY;
			} else {
				Account selectedAccount = openApplications.get(selection - 2);
				Menu M = new AccountApplicationAcceptDenyMenu(selectedAccount);
				return M.doAbstractMenuAction();
			}
		}
	}

}
