package menus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract class Menu implements Navigatable {
	
	private MenuType Type;

	StringBuffer menuTitle = new StringBuffer("");
	StringBuffer menuSubTitle = new StringBuffer("");
	List<StringBuffer> menuOptions;
	
	public Menu(MenuType Type) {
		this.Type = Type;
	}

	private void printTitleSubtitle() {
		printMenuTop();
		System.out.println(menuTitle);
		System.out.println(menuSubTitle);
	}
	
	private void printOptions() {
		for (int i = 0; i < menuOptions.size(); i++) {
			System.out.println(i + " - " + menuOptions.get(i));
		}
	}
	
	MenuReturn doAbstractMenuAction() {
		if(Type == null || Type == MenuType.UNTYPED) {
			//this should only happen while I'm coding, so that unimplemented features don't cause something awful to happen
			return MenuReturn.ERROR;
		}
		menuOptions = new ArrayList<>(Arrays.asList(new StringBuffer("Exit Program")));
		menuTitle = new StringBuffer("");
		menuSubTitle = new StringBuffer("");
		generateTitles();
		populateOptions();
		printTitleSubtitle();
		int selection = 0; //default value if the menu is not designed to give user options, like the registration menu
		if(Type.optioned) {
			printOptions();
			selection = MenuMasterControl.getUserInputInt(0, menuOptions.size() - 1);
		}
		MenuReturn R = doMenuAction(selection);
		if(R == null) {
			R = MenuReturn.ERROR;
		}
		switch(R) {
		case EXIT:
			printMenuBottom();
			return MenuReturn.EXIT;
		case BACK:
			printMenuBottom();
			return MenuReturn.STAY;
		case STAY:
			printMenuBottom();
			return doAbstractMenuAction();
		case GOHOME:
			printMenuBottom();
			switch(Type) {
			case HOME:
				return MenuReturn.STAY;
			default:
				return MenuReturn.GOHOME;
			}
		default:
			System.out.println("Something about this menu is broken or not yet implemented. Returning home!");
			printMenuBottom();
			return MenuReturn.GOHOME;
		}
	}	
	
	private void printMenuTop() {
		System.out.println("##########");
	}
	
	private void printMenuBottom() {
		System.out.println("**********");
	}
	
	void addOption(String option) {
		menuOptions.add(new StringBuffer(option));
	}
}
