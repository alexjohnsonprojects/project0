package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import database.DB;
import users.Admin;
import users.Customer;
import users.Employee;
import users.User;
import users.UserOracleDAO;
import users.UserType;

class UserDAOTests {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		DB.closeConn();
	}

	@BeforeEach
	void setUp() throws Exception {
		DB.truncateAll();
	}

	@AfterEach
	void tearDown() throws Exception {
		DB.truncateAll();
	}

	@Test
	void testInsertUserDAO() {
		UserOracleDAO udao = new UserOracleDAO();
		User uC = new Customer("AlexC", "12345", "111-11-1111");
		User uE = new Employee("AlexE", "12345", "111-11-1111");
		User uA = new Admin("AlexA", "12345", "111-11-1111");
		assertTrue(udao.insertUser(uC));
		assertFalse(udao.insertUser(uC));
		assertTrue(udao.insertUser(uE));
		assertFalse(udao.insertUser(uE));
		assertTrue(udao.insertUser(uA));
		assertFalse(udao.insertUser(uA));
	}

	@Test
	void testUpdateUserDAO() {
		UserOracleDAO udao = new UserOracleDAO();
		User u = new Customer("AlexC", "12345", "111-11-1111");
		User u2 = new Customer("AlexC", "12345", "111-11-1111", 123.45);
		User u3 = new Customer("blah", "54321", "111-11-1111");
		assertTrue(udao.insertUser(u));
		assertTrue(udao.updateUser(u2));
		assertTrue(udao.updateUser(u2));
		assertFalse(udao.updateUser(u3));
		Customer found = (Customer) udao.getUserByUsername("AlexC");
		assertNotNull(found);
		assertEquals(found.getAmountWithdrawnToday(), 123.45);
	}
	
	@Test
	void testFindUserDAO() {
		UserOracleDAO udao = new UserOracleDAO();
		User u = new Customer("AlexC", "12345", "111-11-1111");
		assertTrue(udao.insertUser(u));
		User found = udao.getUserByUsername("AlexC");
		assertNotNull(found);
		User notFound = udao.getUserByUsername("AlexJ");
		assertNull(notFound);
	}

	@Test
	void testCheckExistingUsernameDAO() {
		UserOracleDAO udao = new UserOracleDAO();
		User u = new Customer("AlexC", "12345", "111-11-1111");
		assertTrue(udao.insertUser(u));
		assertTrue(udao.checkExistingUsername("AlexC"));
		assertFalse(udao.checkExistingUsername("AlexJ"));
	}
	
	@Test
	void testCheckExistingSSN() {
		UserOracleDAO udao = new UserOracleDAO();
		User u = new Customer("AlexC", "12345", "111-11-1111");
		assertTrue(udao.insertUser(u));
		assertTrue(udao.checkExistingSSN("111-11-1111",UserType.CUSTOMER));
		assertFalse(udao.checkExistingSSN("111-11-1111",UserType.EMPLOYEE));
		assertFalse(udao.checkExistingSSN("111-21-1111",UserType.CUSTOMER));
	}
	
	@Test
	void testGetAllUsers() {
		UserOracleDAO udao = new UserOracleDAO();
		List<User> empty = udao.getUsersOfType(UserType.CUSTOMER);
		assertEquals(empty.size(), 0);
		User u = new Customer("AlexC", "12345", "111-11-1111");
		User u1 = new Employee("AlexE", "12345", "111-11-1111");
		User u2 = new Employee("AlexE2", "12345", "222-22-2222");
		assertTrue(udao.insertUser(u));
		assertTrue(udao.insertUser(u1));
		assertTrue(udao.insertUser(u2));
		empty = udao.getUsersOfType(UserType.ADMIN);
		List<User> one = udao.getUsersOfType(UserType.CUSTOMER);
		List<User> two = udao.getUsersOfType(UserType.EMPLOYEE);
		assertEquals(empty.size(), 0);
		assertEquals(one.size(), 1);
		assertEquals(two.size(), 2);
		User getSecondEmployee = udao.getUserByUsername("AlexE2");
		assertEquals(getSecondEmployee.getSSN(), two.get(1).getSSN());
	}
}
