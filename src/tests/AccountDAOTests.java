package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import bankingaccounts.Account;
import bankingaccounts.AccountOracleDAO;
import bankingaccounts.AccountStatus;
import database.DB;
import exceptions.DepositTooBigException;
import exceptions.NonpositiveException;
import users.Customer;
import users.UserOracleDAO;

class AccountDAOTests {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		DB.closeConn();
	}

	@BeforeEach
	void setUp() throws Exception {
		DB.truncateAll();
	}

	@AfterEach
	void tearDown() throws Exception {
		DB.truncateAll();
	}

	@Test
	void testInsertAccount() {
		UserOracleDAO udao = new UserOracleDAO();
		AccountOracleDAO adao = new AccountOracleDAO();
		Customer c1 = new Customer("AlexC", "12345", "111-11-1111", 0.0);
		Customer c2 = new Customer("GregC", "12345", "222-22-2222", 0.0);
		udao.insertUser(c1);
		udao.insertUser(c2);
		Account a1 = new Account(0, c1, AccountStatus.APPLIED, 20.0);
		Account a2 = new Account(0, c1, c2, AccountStatus.APPLIED, 0.0);
		assertTrue(adao.insertAccount(a1));
		assertTrue(adao.insertAccount(a1));
		assertTrue(adao.insertAccount(a2));
		assertTrue(adao.insertAccount(a2));
	}
	
	@Test
	void testRetrieveAccount() {
		UserOracleDAO udao = new UserOracleDAO();
		AccountOracleDAO adao = new AccountOracleDAO();
		Customer c1 = new Customer("AlexC", "12345", "111-11-1111", 0.0);
		Customer c2 = new Customer("GregC", "12345", "222-22-2222", 11.0);
		udao.insertUser(c1);
		udao.insertUser(c2);
		Account a1 = new Account(c1, AccountStatus.APPLIED, 20.0);
		Account a2 = new Account(c1, c2, AccountStatus.ACTIVE, 0.0);
		assertTrue(adao.insertAccount(a1));
		assertTrue(adao.insertAccount(a2));
		AccountStatus[] applied = {AccountStatus.APPLIED};
		AccountStatus[] rejected = {AccountStatus.REJECTED};
		AccountStatus[] appliedactive = {AccountStatus.APPLIED, AccountStatus.ACTIVE};
		List<Account> alexappliedaccs = adao.getAllAccounts(c1, applied);
		List<Account> alexrejectedaccs = adao.getAllAccounts(c1, rejected);
		List<Account> alexappliedactiveaccs = adao.getAllAccounts(c1, appliedactive);
		List<Account> gredapplied = adao.getAllAccounts(c2, applied);
		List<Account> gregappliedactive = adao.getAllAccounts(c2, appliedactive);
		assertEquals(1, alexappliedaccs.size());
		assertEquals(0, alexrejectedaccs.size());
		assertEquals(2, alexappliedactiveaccs.size());
		assertEquals(0, gredapplied.size());
		assertEquals(1, gregappliedactive.size());
		assertEquals(20.0, alexappliedaccs.get(0).getBalance());
		assertEquals(11.0, ((Customer) gregappliedactive.get(0).getSecondaryOwner()).getAmountWithdrawnToday());
		assertEquals("111-11-1111", gregappliedactive.get(0).getPrimaryOwner().getSSN());
		assertTrue(adao.insertAccount(a1));
		List<Account> all = adao.getAllAccounts(null, appliedactive);
		assertEquals(3, all.size());
	}
	
	@Test
	void testUpdateAccount() {
		UserOracleDAO udao = new UserOracleDAO();
		AccountOracleDAO adao = new AccountOracleDAO();
		Customer c1 = new Customer("AlexC", "12345", "111-11-1111", 0.0);
		udao.insertUser(c1);
		Account a1 = new Account(c1, AccountStatus.APPLIED, 0.0);
		assertFalse(adao.updateAccount(a1));
		assertTrue(adao.insertAccount(a1));
		AccountStatus[] appliedactive = {AccountStatus.APPLIED, AccountStatus.ACTIVE};
		List<Account> getacc = adao.getAllAccounts(null, appliedactive);
		assertEquals(1, getacc.size());
		a1 = getacc.get(0); //gets account so that it actually has the right ID
		try {
			a1.setStatus(AccountStatus.ACTIVE);
			a1.makeDeposit(100.0);
		} catch (DepositTooBigException|NonpositiveException e) {
			e.printStackTrace();
		}
		assertTrue(adao.updateAccount(a1));
		assertEquals(100.0, getacc.get(0).getBalance());
	}

}
