package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import bankingaccounts.*;
import exceptions.DepositTooBigException;
import exceptions.NonpositiveException;
import users.*;


@Deprecated
class AccountTests {

	Customer C;
	AccountID aid;
	Account A;
	
	
	@BeforeEach
	void setUpAccount() {
		C = new Customer(0, "username", "password", "111-11-1111");
//		aid = Account.createNewAccount(C, AccountStatus.ACTIVE);
//		A = Account.getAllAccountsMap().get(aid);
	}
	
	@Test
	void nullAccountTest( ) {
		if(A == null) {
			fail("Null account!");
		}
	}
	
	@Test
	void makeDespositTest1() {
		try {
			A.makeDeposit(400);
			if(A.getBalance() != 400) {
				fail("Deposit didn't work!");
			}
		} catch (DepositTooBigException|NonpositiveException e) {
			fail("Rejected a valid deposit for the reason: " + e.getMessage());
		}
	}
	
	@Test
	void makeDepositTest2() {
		try {
			A.makeDeposit(-1);
			fail("Accepted a negative deposit!");
		} catch (DepositTooBigException e) {
			fail("Rejected a negative deposit for the reason: " + e.getMessage());
		} catch (NonpositiveException e) {
			//pass!
		}
	}
	
	@Test
	void makeDepositTest3() {
		try {
			A.makeDeposit(Account.getMaxDeposit() + 1);
			fail("Accepted too big of a deposit!");
		} catch (DepositTooBigException e) {
			//pass!
		} catch (NonpositiveException e) {
			fail("Rejected too big of a deposit for the reason: " + e.getMessage());
		}
	}
}
