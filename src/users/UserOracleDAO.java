package users;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import database.DB;

public class UserOracleDAO implements UserDAO {

	Connection conn;
	PreparedStatement ps;

	@Override
	public boolean insertUser(User u) {
		try {
//			conn = DriverManager.getConnection(db_url, user, password);
			String table = "";
			String custAdd = "";
			switch (u.getUserType()) {
			case CUSTOMER:
				table = "customers";
				custAdd = "?, ";
				break;
			case EMPLOYEE:
				table = "employees";
				break;
			case ADMIN:
				table = "admins";
				break;
			}
			String command = "DECLARE new_un_pw_id INT; BEGIN SELECT usernames_passwords_seq.NEXTVAL INTO new_un_pw_id FROM dual; "
					+ "INSERT INTO usernames_passwords VALUES (new_un_pw_id, ?, ?); " + "INSERT INTO " + table
					+ " VALUES (" + table + "_seq.NEXTVAL, ?, " + custAdd + "new_un_pw_id); END;";
			ps = DB.getInstance().conn.prepareStatement(command);
			ps.setString(1, u.getUsername());
			ps.setString(2, u.getPassword());
			ps.setString(3, u.getSSN());
			if(!custAdd.equals("")) {
				ps.setDouble(4,((Customer) u).getAmountWithdrawnToday());
			}
			ps.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
//			try {
//				conn.close();
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
		}
		return false;
	}

	@Override
	public boolean updateUser(User u) {
		// finds user by username
		// right now this ONLY changes a customer's withdrawn_today
		// no other information about any other user or kind of user can change
		try {
//			conn = DriverManager.getConnection(db_url, user, password);
			switch (u.getUserType()) {
			case EMPLOYEE:
			case ADMIN:
				return false;
			default:
				break;
			}
//			String command = "UPDATE (SELECT * FROM " + table
//					+ " c INNER JOIN usernames_passwords up ON c.un_pw_id = up.un_pw_id WHERE up.username = ?)"
//					+ " SET up.username = ?," + " up.password = ?," + " c.ssn = ?" + custAdd;

			String command = "UPDATE customers SET withdrawn_today = ? WHERE un_pw_id = (SELECT un_pw_id FROM usernames_passwords WHERE username = ?)";
			ps = DB.getInstance().conn.prepareStatement(command);
			ps.setDouble(1, ((Customer) u ).getAmountWithdrawnToday());
			ps.setString(2, u.getUsername());
			return ps.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
//			try {
//				conn.close();
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
		}
		return false;
	}

	@Override
	public User getUserByUsername(String username, UserType UType) {
		User u = null;
		String password;
		String ssn;
		double withdrawnToday;
		ResultSet rs;
		String[] commands = { "SELECT up.username, up.password, c.ssn, c.withdrawn_today "
				+ "FROM (customers c INNER JOIN usernames_passwords up ON c.un_pw_id = up.un_pw_id) WHERE up.username = ?",
				"SELECT up.username, up.password, e.ssn "
						+ "FROM (employees e INNER JOIN usernames_passwords up ON e.un_pw_id = up.un_pw_id) WHERE up.username = ?",
				"SELECT up.username, up.password, a.ssn "
						+ "FROM (admins a INNER JOIN usernames_passwords up ON a.un_pw_id = up.un_pw_id) WHERE up.username = ?" };
		try {
			for (int i = 0; i < 3; i++) {
				int k;
				if (UType == null) {
					k = i;
				} else {
					k = UType.ordinal();
				}
				ps = DB.getInstance().conn.prepareStatement(commands[k]);
				ps.setString(1, username);
				rs = ps.executeQuery();
				if (rs.next()) {
					password = rs.getString(2);
					ssn = rs.getString(3);
					switch (i) {
					case 0:
						withdrawnToday = rs.getDouble(4);
						u = new Customer(username, password, ssn, withdrawnToday);
						break;
					case 1:
						u = new Employee(username, password, ssn);
						break;
					case 2:
						u = new Admin(username, password, ssn);
						break;
					}
					break;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return u;
	}

	@Override
	public boolean checkExistingUsername(String username) {
		return getUserByUsername(username) != null;
	}

	@Override
	public boolean checkExistingUsername(String username, UserType UType) {
		return getUserByUsername(username, UType) != null;
	}

	@Override
	public boolean checkExistingSSN(String ssn, UserType UType) {
		ResultSet rs = null;
		String table = "";
		switch (UType) {
		case CUSTOMER:
			table = "customers";
			break;
		case EMPLOYEE:
			table = "employees";
			break;
		case ADMIN:
			table = "admins";
			break;
		}
		try {
			ps = DB.getInstance().conn.prepareStatement("SELECT COUNT(1) FROM " + table + " WHERE ssn = ?");
			ps.setString(1, ssn);
			rs = ps.executeQuery();
			rs.next();
			return rs.getInt(1) > 0;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return false;
	}

	@Override
	public List<User> getUsersOfType(UserType UType) {
		List<User> selectedUsers = new ArrayList<User>();
		ResultSet rs = null;
		String table = "";
		String custAdd = "";
		switch (UType) {
		case CUSTOMER:
			table = "customers";
			custAdd = ", t.withdrawn_today";
			break;
		case EMPLOYEE:
			table = "employees";
			break;
		case ADMIN:
			table = "admins";
			break;
		}
		String command = "SELECT up.username, up.password, t.ssn" + custAdd + " FROM " + table
				+ " t INNER JOIN usernames_passwords up ON t.un_pw_id = up.un_pw_id";
		rs = DB.query(command);
		try {
			while (rs.next()) {
				User u = null;
				String username = rs.getString(1);
				String password = rs.getString(2);
				String ssn = rs.getString(3);
				switch (UType) {
				case CUSTOMER:
					double withdrawnToday = rs.getDouble(4);
					u = new Customer(username, password, ssn, withdrawnToday);
					break;
				case EMPLOYEE:
					u = new Employee(username, password, ssn);
					break;
				case ADMIN:
					u = new Admin(username, password, ssn);
					break;
				}
				selectedUsers.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return selectedUsers;
	}

	@Override
	public User getUserByUsername(String username) {
		return getUserByUsername(username, null);
	}
	

	@Override
	public User getUserByID(int userID, UserType UType) {
		User u = null;
		String username;
		String password;
		String ssn;
		double withdrawnToday;
		ResultSet rs;
		String[] commands = { "SELECT c.customer_id, up.username, up.password, c.ssn, c.withdrawn_today "
				+ "FROM (customers c INNER JOIN usernames_passwords up ON c.un_pw_id = up.un_pw_id) WHERE c.customer_id = ?",
				"SELECT e.employee_id, up.username, up.password, e.ssn "
						+ "FROM (employees e INNER JOIN usernames_passwords up ON e.un_pw_id = up.un_pw_id) WHERE e.employee_id = ?",
				"SELECT a.admin_id, up.username, up.password, a.ssn "
						+ "FROM (admins a INNER JOIN usernames_passwords up ON a.un_pw_id = up.un_pw_id) WHERE a.admin_id = ?" };
		try {
			for (int i = 0; i < 3; i++) {
				ps = DB.getInstance().conn.prepareStatement(commands[UType.ordinal()]);
				ps.setInt(1, userID);
				rs = ps.executeQuery();
				if (rs.next()) {
					username = rs.getString(2);
					password = rs.getString(3);
					ssn = rs.getString(4);
					switch (i) {
					case 0:
						withdrawnToday = rs.getDouble(5);
						u = new Customer(userID, username, password, ssn, withdrawnToday);
						break;
					case 1:
						u = new Employee(userID, username, password, ssn);
						break;
					case 2:
						u = new Admin(userID, username, password, ssn);
						break;
					}
					break;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return u;
	}

}
