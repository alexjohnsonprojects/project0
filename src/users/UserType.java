package users;

public enum UserType {
	CUSTOMER("Customer"), EMPLOYEE("Employee"), ADMIN("Admin");
	
	private String name;
	

	private UserType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
