package users;

import java.util.List;

public interface UserDAO {
	public boolean insertUser(User u);
	boolean updateUser(User u);
	public User getUserByUsername(String username);
	public User getUserByUsername(String username, UserType UTYpe);
	public User getUserByID(int userID, UserType UType);
	public boolean checkExistingUsername(String username);
	public boolean checkExistingUsername(String username, UserType UType);
	public boolean checkExistingSSN(String ssn, UserType UType);
	public List<User> getUsersOfType(UserType UType);
	
}
