package users;

@SuppressWarnings("serial")
public class Admin extends Employee {
	
	private int adminID;
	
	public Admin(int adminID, String username, String password, String ssn) {
		super(username, password, ssn, UserType.ADMIN);
		this.adminID = adminID;
	}
	
	public Admin(String username, String password, String ssn) {
		this(0, username, password, ssn);
	}
}
