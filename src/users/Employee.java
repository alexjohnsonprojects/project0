package users;

public class Employee extends User{
	
	private int employeeID;
	
	public Employee(String username, String password, String ssn) {
		this(0, username, password, ssn);
	}
	
	public Employee(String username, String password, String ssn, UserType UType) {
		this(0, username, password, ssn, UType);
	}
	
	public Employee(int employeeID, String username, String password, String ssn) {
		super(username, password, ssn, UserType.EMPLOYEE);
		this.employeeID = employeeID;
	}
	
	public Employee(int employeeID, String username, String password, String ssn, UserType UType) {
		super(username, password, ssn, UType);
		this.employeeID = employeeID;
	}
}
