package users;

import java.io.Serializable;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.Iterator;
import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;

//import bankingaccounts.Account;
//import bankingaccounts.AccountDAO;
//import bankingaccounts.AccountID;
//import database.DB;
//
//import java.util.Set;

import exceptions.BadDataException;
import exceptions.NonexistingUsernameException;

@SuppressWarnings("serial")
public abstract class User implements Serializable {

	
//	@Deprecated
//	private static Map<String, User> allUsers = new HashMap<>();
	
	
	private static final int NUM_SPACES_IN_GOOD_DATA = 3;
	
	private StringBuffer ssn;
	private StringBuffer username;
	private StringBuffer password;

	private UserType UType;
	
	public User(String username, String password, String ssn, UserType UType) {
		this.username = new StringBuffer(username);
		this.password = new StringBuffer(password);
		this.ssn = new StringBuffer(ssn);
		this.UType = UType;
	}

	public static boolean createAndAddNewUser(String username, String password, String ssn, UserType UType) {
		User U;
		switch (UType) {
		default:
		case CUSTOMER:
			U = new Customer(username, password, ssn);
			break;
		case EMPLOYEE:
			U = new Employee(username, password, ssn);
			break;
		case ADMIN:
			U = new Admin(username, password, ssn);
			break;
		}
//		addUser(U);
//		return DB.createNewUser(username, password, ssn, UType);
		UserDAO udao = new UserOracleDAO();
		return udao.insertUser(U);
	}

	public static User getUserByUsername(String username) throws NonexistingUsernameException {
		// there should be only a single user with this username!
//		User U = getAllUsers().get(username);
//		if (U == null) {
//			throw new NonexistingUsernameException();
//		}
		UserDAO udao = new UserOracleDAO();
		User u = udao.getUserByUsername(username);
		if(u == null) {
			throw new NonexistingUsernameException();
		}
		return u;
	}

	public static boolean ssnAlreadyExistsSameUserType(String ssn, UserType UType) {
//		for (User u : getAllUsersValues()) {
//			if (u.ssn.toString().equals(ssnToCheck) && u.UType == UType) {
//				return true;
//			}
//		}
//		return false;
//		return DB.verifySSNIsUnused(ssn, UType);
		UserDAO udao = new UserOracleDAO();
		return udao.checkExistingSSN(ssn, UType);
	}

	public static boolean usernameAlreadyExists(String usernameToCheck) {
//		return !DB.verifyUsernameIsUnused(usernameToCheck);
//		return getAllUsers().get(usernameToCheck) != null;
		UserDAO udao = new UserOracleDAO();
		return udao.checkExistingUsername(usernameToCheck);
	}

	public static boolean usernameAlreadyExists(String usernameToCheck, UserType UType) {
		// returns false unless the user with this username also has this type
//		User U = getAllUsers().get(usernameToCheck);
//		return U != null && U.getUserType() == UType;
		UserDAO udao = new UserOracleDAO();
		return udao.checkExistingUsername(usernameToCheck, UType);
	}

	@Deprecated
	public static boolean addUser(User U) {
//		getAllUsers().put(U.getUsername(), U);
		UserDAO udao = new UserOracleDAO();
		return udao.insertUser(U);
	}

	@Override
	public boolean equals(Object U) {
		// usernames must be unique, so this should work...
		return username.toString().equals(((User) U).getUsername());
	}

	public static boolean checkFormattedSSN(String candidateSSN) {
		// I wrote this method before learning regex, but it should still work
		if (candidateSSN.length() != 11) {
			return false;
		}
		String s1 = candidateSSN.substring(0, 3);
		String s2 = candidateSSN.substring(4, 6);
		String s3 = candidateSSN.substring(7, 11);
		String dashes = "" + candidateSSN.charAt(3) + candidateSSN.charAt(6);
		if (!dashes.equals("--")) {
			return false;
		}
		try {
			Integer.parseInt(s1);
			Integer.parseInt(s2);
			Integer.parseInt(s3);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	public String getUsername() {
		return username.toString();
	}

	public boolean checkPassword(String candidatePassword) {
		return password.toString().equals(candidatePassword);
	}

	public UserType getUserType() {
		return UType;
	}

//	@Deprecated
//	public static Collection<User> getAllUsersValues() {
//		return getAllUsers().values();
//	}

//	@Deprecated
//	public static void setAllUsers(Map<String, User> allUsers) {
//		User.allUsers = allUsers;
//	}

	public String getSSN() {
		return ssn.toString();
	}

	public String getDisplayName() {
		return username.toString();
	}

	public String getDisplayString() {
		return "[username = " + getUsername() + ", ssn = " + getSSN() + "]";
	}

//	@Deprecated
//	public static Map<String, User> getAllUsers() {
//		return allUsers;
//	}

	public static List<User> getAllUsersOfType(UserType UType) {
//		List<User> allUsersOfType = new ArrayList<>();
//		for (User u : getAllUsers().values()) {
//			if (u.getUserType() == type) {
//				allUsersOfType.add(u);
//			}
//		}
		UserDAO udao = new UserOracleDAO();
		return udao.getUsersOfType(UType);
	}

	public String getPassword() {
		return password.toString();
	}

	
//	@Deprecated
//	public static String allUsersToString() {
//		StringBuffer encoded = new StringBuffer();
//		Iterator<User> iter = getAllUsersValues().iterator();
//		while (iter.hasNext()) {
//			User u = iter.next();
//			encoded.append('[');
//			encoded.append(u.username);
//			encoded.append(' ');
//			encoded.append(u.password);
//			encoded.append(' ');
//			encoded.append(u.ssn);
//			encoded.append(' ');
//			encoded.append(u.UType.ordinal());
//			encoded.append(']');
//			if (iter.hasNext()) {
//				encoded.append('\n');
//			}
//		}
//		return encoded.toString();
//	}

	@Deprecated
	public static void createAndAddUserFromEncodedUser(String encodedUser) throws BadDataException {
		if (!(encodedUser.charAt(0) + "" + encodedUser.charAt(encodedUser.length() - 1)).equals("[]")) {
			throw new BadDataException();
		}
		String encodedUserCrop = encodedUser.substring(1, encodedUser.length() - 1);
		int[] spaceIndices = new int[NUM_SPACES_IN_GOOD_DATA];
		int numSpaces = 0;
		for (int i = 0; i < encodedUserCrop.length(); i++) {
			if (encodedUserCrop.charAt(i) == ' ') {
				if (numSpaces >= NUM_SPACES_IN_GOOD_DATA) {
					throw new BadDataException();
				}
				spaceIndices[numSpaces] = i;
				numSpaces++;
			}
		}
		String username = encodedUserCrop.substring(0, spaceIndices[0]);
		String password = encodedUserCrop.substring(spaceIndices[0] + 1, spaceIndices[1]);
		String ssn = encodedUserCrop.substring(spaceIndices[1] + 1, spaceIndices[2]);
		String typeOrdinal = encodedUserCrop.substring(spaceIndices[2] + 1);
		int typeOrdinalInt;
		typeOrdinalInt = Integer.parseInt(typeOrdinal);
		if (typeOrdinalInt < 0 || typeOrdinalInt > 2) {
			throw new BadDataException();
		}
		UserType UType = UserType.values()[typeOrdinalInt];
		User.createAndAddNewUser(username, password, ssn, UType);
	}
}
