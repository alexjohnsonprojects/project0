package users;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import bankingaccounts.AccountID;

@SuppressWarnings("serial")
public class Customer extends User implements Serializable {
	
	
	@Deprecated
	private List<AccountID> accountIDs = new ArrayList<>();
	
	
	private int customerID;
	
	
	private double amountWithdrawnToday;
	
	public Customer(String username, String password, String ssn) {
		super(username, password, ssn, UserType.CUSTOMER);
	}
	
	public Customer(int customerID, String username, String password, String ssn) {
		super(username, password, ssn, UserType.CUSTOMER);
		this.customerID = customerID;
	}
	
	public Customer(int customerID, String username, String password, String ssn, Double amountWithdrawnToday) {
		super(username, password, ssn, UserType.CUSTOMER);
		this.customerID = customerID;
		this.amountWithdrawnToday = amountWithdrawnToday;
	}
	
	public Customer(String username, String password, String ssn, Double amountWithdrawnToday) {
		super(username, password, ssn, UserType.CUSTOMER);
		this.amountWithdrawnToday = amountWithdrawnToday;
	}

	
	@Deprecated
	public void addAccountID(AccountID aid) {
		accountIDs.add(aid);
	}
	
	
	@Deprecated
	public List<AccountID> getAIDs() {
		return accountIDs;
	}
	
	public String getFullCustomerDisplayString() {
		return "[username = " + getUsername() + ", password = " + getPassword() + ", ssn = " + getSSN() + "]";
	}

	public double getAmountWithdrawnToday() {
		return amountWithdrawnToday;
	}

	public void setAmountWithdrawnToday(double amountWithdrawnToday) {
		this.amountWithdrawnToday = amountWithdrawnToday;
	}

	public void addToWithdrawToday(double withdraw) {
		amountWithdrawnToday += withdraw;
	}
}
