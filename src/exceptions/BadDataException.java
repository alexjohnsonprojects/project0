package exceptions;

public class BadDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		return "There is something wrong with the data being read from the file! Unable to load.";
	}

}
