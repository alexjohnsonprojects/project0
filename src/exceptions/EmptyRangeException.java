package exceptions;

public class EmptyRangeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		return "Requesting user input to lie in an empty range!";
	}
	
}
