package exceptions;

public class SpaceInInputException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		return "You cannot use spaces!";
	}
	
	public String getMessage(String append) {
		return "You cannot use spaces in your " + append + "!";
	}

}
