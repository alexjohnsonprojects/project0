package exceptions;

public class WithdrawTooBigException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		return "That withdraw would put you over your daily limit!";
	}

}
