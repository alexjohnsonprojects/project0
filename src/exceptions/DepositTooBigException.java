package exceptions;

import bankingaccounts.Account;

public class DepositTooBigException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		return "You cannot deposit more than " + Account.getMaxDeposit() + " at one time!";
	}

}
