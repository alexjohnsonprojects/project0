package exceptions;

public class BadPasswordException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BadPasswordException () {
		super("Password is too short!");
	}

}
