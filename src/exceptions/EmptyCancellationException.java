package exceptions;

public class EmptyCancellationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EmptyCancellationException() {
		super("Empty input, operation cancelled");
	}
}
