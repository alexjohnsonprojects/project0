package exceptions;

public class SecondUserIsSelfException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Override
	public String getMessage() {
		return "The second user cannot be yourself!";
	}
}
