package exceptions;

public class NonexistingUsernameException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public NonexistingUsernameException() {
		super("No valid user with this username exists!");
	}
}
