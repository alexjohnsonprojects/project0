package exceptions;

public class BadSSNException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BadSSNException() {
		super("Invalid SSN!");
	}
	
}
