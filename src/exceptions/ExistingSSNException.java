package exceptions;

public class ExistingSSNException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExistingSSNException() {
		super("You already have an account under this SSN.");
	}

}
