package database;

import java.sql.*;

import bankingaccounts.AccountStatus;
import bankingaccounts.AccountType;
import exceptions.NonexistingUsernameException;
import users.UserType;

public class DB {

	static final String user = "admin";
	static final String password = "12345678";
	static final String db_url = "jdbc:oracle:thin:@database-1.cmf2toyaz9wt.us-east-2.rds.amazonaws.com:1521:orcl";
	
	private static DB inst = null;

	public Connection conn;
	public Statement stmt;
	
	private DB() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(db_url, user, password);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static DB getInstance() {
		if(inst == null) {
			inst = new DB();
		}
		return inst;
	}
	
	public static void closeConn() {
		try {
			getInstance().conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void truncateAll() {
		String[] commands = {"BEGIN\r\n"
				+ "  FOR c IN (SELECT table_name, constraint_name FROM user_constraints WHERE constraint_type = 'R')\r\n"
				+ "  LOOP\r\n"
				+ "    EXECUTE IMMEDIATE ('alter table ' || c.table_name || ' disable constraint ' || c.constraint_name);\r\n"
				+ "  END LOOP;\r\n"
				+ "  EXECUTE IMMEDIATE('truncate table accounts');\r\n"
				+ " EXECUTE IMMEDIATE('truncate table customers');\r\n"
				+ "EXECUTE IMMEDIATE('truncate table employees');\r\n"
				+ "EXECUTE IMMEDIATE('truncate table admins');\r\n"
				+ "EXECUTE IMMEDIATE('truncate table usernames_passwords');\r\n"
				+ "EXECUTE IMMEDIATE('truncate table accounts_customers');\r\n"
				+ "  FOR c IN (SELECT table_name, constraint_name FROM user_constraints WHERE constraint_type = 'R')\r\n"
				+ "  LOOP\r\n"
				+ "    EXECUTE IMMEDIATE ('alter table ' || c.table_name || ' enable constraint ' || c.constraint_name);\r\n"
				+ "  END LOOP;\r\n"
				+ "END;"};
		update(commands);
	}

	@Deprecated
	public static String getPasswordFromUsername(String username) throws NonexistingUsernameException {
		ResultSet rs = query(
				"SELECT up.password FROM (customers c INNER JOIN usernames_passwords up ON c.un_pw_id = up.un_pw_id) WHERE up.username = '"
						+ username + "'");
		try {
			if (rs.next()) {
				return rs.getString(1);
			}
			throw new NonexistingUsernameException();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				getInstance().conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	@Deprecated	
	public static boolean createNewAccountApplication(String primaryOwner, String secondaryOwner, AccountType AType) {
		String command = "";
		switch (AType) {
		case CHECKING:
			command = "DECLARE new_acc_id INT;\n" + "        found_cust_id INT;\n" + "\n" + "BEGIN\n"
					+ "    SELECT accounts_seq.NEXTVAL INTO new_acc_id FROM dual;\n"
					+ "    SELECT c.customer_id INTO found_cust_id FROM (usernames_passwords up INNER JOIN customers c ON c.un_pw_id = up.un_pw_id) "
					+ "    WHERE up.username = '" + primaryOwner + "';\n"
					+ "    INSERT INTO accounts VALUES (new_acc_id, found_cust_id, null, 0, "
					+ AccountStatus.APPLIED.ordinal() + ", " + AType.ordinal() + ");\n"
					+ "    INSERT INTO accounts_customers VALUES (accounts_customers_seq.NEXTVAL, found_cust_id, new_acc_id);\n"
					+ "END;";
			break;
		case JOINT_CHECKING:
			command = "DECLARE new_acc_id INT;\n" + "        found_pcust_id INT;\n" + "        found_scust_id INT;\n"
					+ "\n" + "BEGIN\n" + "    SELECT accounts_seq.NEXTVAL INTO new_acc_id FROM dual;\n"
					+ "    SELECT c.customer_id INTO found_pcust_id FROM (usernames_passwords up INNER JOIN customers c ON c.un_pw_id = up.un_pw_id) WHERE up.username = '"
					+ primaryOwner + "';\n"
					+ "    SELECT c.customer_id INTO found_scust_id FROM (usernames_passwords up INNER JOIN customers c ON c.un_pw_id = up.un_pw_id) WHERE up.username = '"
					+ secondaryOwner + "';\n"
					+ "    INSERT INTO accounts VALUES (new_acc_id, found_pcust_id, found_scust_id, 0, "
					+ AccountStatus.APPLIED.ordinal() + ", " + AType.ordinal() + ");\n"
					+ "    INSERT INTO accounts_customers VALUES (accounts_customers_seq.NEXTVAL, found_pcust_id, new_acc_id);\n"
					+ "    INSERT INTO accounts_customers VALUES (accounts_customers_seq.NEXTVAL, found_scust_id, new_acc_id);\n"
					+ "END;";
			break;
		}
		return update(command);
	}

	@Deprecated
	public static boolean createNewUser(String username, String password, String ssn, UserType UType) {
		String command = "";
		switch (UType) {
		case CUSTOMER:
			command = "DECLARE new_un_pw_id INT; BEGIN SELECT usernames_passwords_seq.NEXTVAL INTO new_un_pw_id FROM dual; "
					+ "INSERT INTO usernames_passwords VALUES (new_un_pw_id, \'" + username + "\', \'" + password
					+ "\'); " + "INSERT INTO customers VALUES (customers_seq.NEXTVAL, \'" + ssn
					+ "\', 0, new_un_pw_id); END;";
			break;
		case EMPLOYEE:
			command = "DECLARE new_un_pw_id INT; BEGIN SELECT usernames_passwords_seq.NEXTVAL INTO new_un_pw_id FROM dual; "
					+ "INSERT INTO usernames_passwords VALUES (new_un_pw_id, \'" + username + "\', \'" + password
					+ "\'); " + "INSERT INTO employees VALUES (employees_seq.NEXTVAL, \'" + ssn
					+ "\', new_un_pw_id); END;";
			break;
		case ADMIN:
			command = "DECLARE new_un_pw_id INT; BEGIN SELECT usernames_passwords_seq.NEXTVAL INTO new_un_pw_id FROM dual; "
					+ "INSERT INTO usernames_passwords VALUES (new_un_pw_id, \'" + username + "\', \'" + password
					+ "\'); " + "INSERT INTO admins VALUES (admins_seq.NEXTVAL, \'" + ssn + "\', new_un_pw_id); END;";
			break;
		}
		return update(command);
	}

	public static ResultSet query(String command) {
		ResultSet rs = null;
		try {
			getInstance().stmt = getInstance().conn.createStatement();
			rs = getInstance().stmt.executeQuery(command);
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	public static boolean update(String command) {
		String[] commands = {command};
		return update(commands);
	}
	
	public static boolean update(String[] commands) {
		try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			conn = DriverManager.getConnection(db_url, user, password);
			getInstance().stmt = getInstance().conn.createStatement();
			for (String c : commands) {
				getInstance().stmt.execute(c);
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
//			try {
//				conn.close();
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
		}
		return false;
	}

	@Deprecated
	public static boolean verifyUsernameIsUnused(String username) {
		ResultSet rs = query("SELECT COUNT(1) FROM usernames_passwords WHERE username = '" + username + "'");
		try {
			rs.next();
			return rs.getInt(1) == 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				getInstance().conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Deprecated
	public static boolean verifyUsernameExistsOfType(String username, UserType UType) {
		ResultSet rs = null;
		switch (UType) {
		case CUSTOMER:
			rs = query(
					"SELECT COUNT(1) FROM (customers c INNER JOIN usernames_passwords up ON c.un_pw_id = up.un_pw_id) WHERE up.username = '"
							+ username + "'");
			break;
		case EMPLOYEE:
			rs = query(
					"SELECT COUNT(1) FROM (employees c INNER JOIN usernames_passwords up ON c.un_pw_id = up.un_pw_id) WHERE up.username = '"
							+ username + "'");
			break;
		case ADMIN:
			rs = query(
					"SELECT COUNT(1) FROM (admins c INNER JOIN usernames_passwords up ON c.un_pw_id = up.un_pw_id) WHERE up.username = '"
							+ username + "'");
			break;
		}
		try {
			rs.next();
			return rs.getInt(1) > 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				getInstance().conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Deprecated
	public static boolean verifySSNIsUnused(String ssn, UserType UType) {
		ResultSet rs = null;
		switch (UType) {
		case CUSTOMER:
			rs = query("SELECT COUNT(1) FROM customers WHERE ssn = '" + ssn + "'");
			break;
		case EMPLOYEE:
			rs = query("SELECT COUNT(1) FROM employees WHERE ssn = '" + ssn + "'");
			break;
		case ADMIN:
			rs = query("SELECT COUNT(1) FROM admins WHERE ssn = '" + ssn + "'");
			break;
		}
		try {
			rs.next();
			return rs.getInt(1) == 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				getInstance().conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
