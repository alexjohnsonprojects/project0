package bankingaccounts;

import java.util.List;

import users.Customer;

public interface AccountDAO {
	public boolean insertAccount(Account a);
	public boolean updateAccount(Account a);
	public List<Account> getAllAccounts(Customer c, AccountStatus[] accountStatuses);
}
