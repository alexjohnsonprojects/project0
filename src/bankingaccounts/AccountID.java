package bankingaccounts;

import java.io.Serializable;

@Deprecated
public class AccountID implements Serializable {
	
	/**
	 * this method is a bit silly because I want to abstract it away, to make it easier to upgrade account IDs to something more sophisticated
	 */
	
	public int ID;
	
	public AccountID(int ID) {
		this.ID = ID;
	}
	
	@Override
	public String toString() {
		return "" + ID;
	}
	
	@Override
	public int hashCode() {
		return ID;
	}
	
	@Override
	public boolean equals(Object aid) {
		return (aid instanceof AccountID) & (((AccountID) aid).ID == ID);
	}

	public static Object getIDFromInt(int intID) {
		return new AccountID(intID);
	}
}
