package bankingaccounts;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.DB;
import users.Customer;
import users.UserOracleDAO;
import users.UserType;

public class AccountOracleDAO implements AccountDAO {

	@Override
	public boolean insertAccount(Account a) {
		// returns false if something went wrong
		// IGNORES THE ACCOUNT ID IN THE OBJECT, since this is a new account it will
		// generate one
		String command = "";
		if (a.getPrimaryOwner() == null) {
			return false;
		}
		if (a.getSecondaryOwner() == null) {
			command = "CALL NEWCHECK(?, ?, ?, ?)";
//			command = "DECLARE new_acc_id INT;\n" + "        found_cust_id INT;\n" + "\n" + "BEGIN\n"
//					+ "    SELECT accounts_seq.NEXTVAL INTO new_acc_id FROM dual;\n"
//					+ "    SELECT c.customer_id INTO found_cust_id FROM (usernames_passwords up INNER JOIN customers c ON c.un_pw_id = up.un_pw_id) "
//					+ "    WHERE up.username = ?;\n"
//					+ "    INSERT INTO accounts VALUES (new_acc_id, found_cust_id, null, ?, ?, ?);\n"
//					+ "    INSERT INTO accounts_customers VALUES (accounts_customers_seq.NEXTVAL, found_cust_id, new_acc_id);\n"
//					+ "END;";
		} else {
			command = "CALL NEWJOINT(?, ?, ?, ?, ?)";
//			command = "DECLARE new_acc_id INT;\n" + "        found_pcust_id INT;\n" + "        found_scust_id INT;\n"
//					+ "\n" + "BEGIN\n" + "    SELECT accounts_seq.NEXTVAL INTO new_acc_id FROM dual;\n"
//					+ "    SELECT c.customer_id INTO found_pcust_id FROM (usernames_passwords up INNER JOIN customers c ON c.un_pw_id = up.un_pw_id) WHERE up.username = ?;\n"
//					+ "    SELECT c.customer_id INTO found_scust_id FROM (usernames_passwords up INNER JOIN customers c ON c.un_pw_id = up.un_pw_id) WHERE up.username = ?;\n"
//					+ "    INSERT INTO accounts VALUES (new_acc_id, found_pcust_id, found_scust_id, ?, ?, ?);\n"
//					+ "    INSERT INTO accounts_customers VALUES (accounts_customers_seq.NEXTVAL, found_pcust_id, new_acc_id);\n"
//					+ "    INSERT INTO accounts_customers VALUES (accounts_customers_seq.NEXTVAL, found_scust_id, new_acc_id);\n"
//					+ "END;";
		}
		try {
			PreparedStatement ps = DB.getInstance().conn.prepareStatement(command);
			int k = 1;
			ps.setString(k++, a.getPrimaryOwner().getUsername());
			if (a.getType() == AccountType.JOINT_CHECKING) {
				ps.setString(k++, a.getSecondaryOwner().getUsername());
			}
			ps.setDouble(k++, a.getBalance());
			ps.setInt(k++, a.getStatus().ordinal());
			ps.setInt(k++, a.getType().ordinal());
			ps.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean updateAccount(Account a) {
		// finds by the accountID, then updates balance and account status
		// for current business logic these are the only two things that can change
		// would cause major problems if other things changed
		// returns false if something went wrong
		PreparedStatement ps;
		try {
			String command = "UPDATE accounts SET balance = ?, account_status = ? WHERE account_id = ?";
			ps = DB.getInstance().conn.prepareStatement(command);
			ps.setDouble(1, a.getBalance());
			ps.setInt(2, a.getStatus().ordinal());
			ps.setInt(3, a.getAccountID());
			return ps.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Account> getAllAccounts(Customer c, AccountStatus[] accountStatuses) {
		// returns accounts of customer c if not null, otherwise all accounts
		// will only return accounts in accountStatuses!
		List<Account> selectedAccounts = new ArrayList<Account>();
		UserOracleDAO udao = new UserOracleDAO();
		if (accountStatuses == null || accountStatuses.length == 0
				|| (c != null && !udao.checkExistingUsername(c.getUsername(), UserType.CUSTOMER))) {
			return selectedAccounts;
		}
		String command;
		if (c != null) {
			command = "SELECT a.account_id, a.primary_owner_id, a.secondary_owner_id, a.balance, a.account_status, a.account_type FROM \n"
					+ "    (usernames_passwords up INNER JOIN  \n" + "        (customers c INNER JOIN \n"
					+ "            (accounts a INNER JOIN accounts_customers ac\n"
					+ "            ON a.account_id = ac.account_id)\n" + "        ON c.customer_id = ac.customer_id)\n"
					+ "    ON up.un_pw_id = c.un_pw_id) WHERE up.username = ? AND (";
		} else {
			command = "SELECT * FROM accounts WHERE (";
		}
		for (int i = 0; i < accountStatuses.length; i++) {
			if(c != null) {
				command += "a.";
			}
			command += "account_status = " + accountStatuses[i].ordinal();
			if (i < accountStatuses.length - 1) {
				command += " OR ";
			} else {
				command += ")";
			}
		}
		try {
			PreparedStatement ps = DB.getInstance().conn.prepareStatement(command);
			if (c != null) {
				ps.setString(1, c.getUsername());
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int accountID = rs.getInt(1);
				int primaryOwnerID = rs.getInt(2);
				int secondaryOwnerID = rs.getInt(3);
				double balance = rs.getDouble(4);
				AccountStatus as = AccountStatus.values()[rs.getInt(5)];
				AccountType at = AccountType.values()[rs.getInt(6)];
				Customer primaryOwner = (Customer) udao.getUserByID(primaryOwnerID, UserType.CUSTOMER);
				Customer secondaryOwner;
				Account a;
				if (at == AccountType.JOINT_CHECKING) {
					secondaryOwner = (Customer) udao.getUserByID(secondaryOwnerID, UserType.CUSTOMER);
					a = new Account(accountID, primaryOwner, secondaryOwner, as, balance);
				} else {
					a = new Account(accountID, primaryOwner, as, balance);
				}
				selectedAccounts.add(a);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return selectedAccounts;
	}

}
